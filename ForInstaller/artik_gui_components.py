import tkinter
import socket


import matplotlib.pyplot as plt

# tkinter
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

# PySide2
from PySide2.QtWidgets import *
from matplotlib.backends.backend_qt5agg import (
        FigureCanvas, NavigationToolbar2QT as NavigationToolbar)

from matplotlib.figure import Figure


class artikPlot:
    def __init__(self, root, figsize, place_x, place_y):
        fig = Figure(figsize=figsize)
        ax = fig.add_subplot(111)
        self.graph = FigureCanvasTkAgg(fig, master=root)
        self.graph.get_tk_widget().place(x=place_x, y=place_y)
        self.ax = ax


    def set_magnetometer_description(self):
        self.ax.set_xlabel('IMU Data: {}'.format(''))
        self.ax.set_ylabel('Magnetometer')


    def remove_lines(self):
        self.ax.lines.pop(0)
        self.ax.lines.pop(0)


class artikCanvas:
    def __init__(self, master, place_x, place_y):
        # Canvas do wyświetlania kamery
        self.frame = tkinter.Frame(master, width=640, height=480)
        self.frame.place(x=place_x, y=place_y)
        self.fig = tkinter.Canvas(self.frame, width=640, height=480, bg='red')
        self.fig.pack()

    def update_image(self, image):
        self.fig.create_image(0, 0, image=image, anchor=tkinter.NW)


class MplWidget(QWidget):

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)

        self.canvas = FigureCanvas(Figure())

        vertical_layout = QVBoxLayout()
        vertical_layout.addWidget(self.canvas)
        vertical_layout.addWidget(NavigationToolbar(self.canvas, self))

        self.canvas.axes = self.canvas.figure.add_subplot(111)
        self.canvas.axes.set_xlabel("X position")
        self.canvas.axes.set_ylabel("Y position")
        # self.canvas.axes.xlim(0, 640)
        # self.canvas.axes.ylim(0, 480)
        self.canvas.axes.set_title("Robot position relative to IMU and camera image")
        self.canvas.figure.subplots_adjust(top=0.912,
                                            bottom=0.117,
                                            left=0.091,
                                            right=0.958)
        self.setLayout(vertical_layout)


def is_valid_ipv4_address(address):
    try:
        socket.inet_pton(socket.AF_INET, address)
    except AttributeError:
        try:
            socket.inet_aton(address)
        except socket.error:
            return False
        return address.count('.') == 3
    except socket.error:
        return False

    return True

def is_valid_port(port):
    try:
        port = int(port)
    except ValueError:
        return False
    try:
        if 1 <= port <= 65535:
            return True
        else:
            raise ValueError
    except ValueError:
        return False