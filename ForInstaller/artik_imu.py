import time
import math
import socket
from PySide2.QtCore import *
import select

class one_record_IMU:
    def __init__(self, message):
        message = message.split(' ')
        self.imu_angle = float(message[0])
        self.imu_x = float(message[1])
        self.imu_y = float(message[2])


class IMUSignals(QObject):
    success = Signal(float, float, float)
    info = Signal(str)
    connection_error = Signal(str)
    wrong_frame_error = Signal(str)


class IMU(QObject):
    def __init__(self, ip_port):
        super().__init__()

        self.connected = False
        self.signal = IMUSignals()

        self.isRunning = True
        self.ip_port = ip_port
        self.complete = False
        self.socket = socket.socket()
        try:
            self.socket.bind(self.ip_port)
            self.socket.listen(1)
        except:
            self.isRunning = False
            self.signal.connection_error.emit("IMU error: Connection loss")

        # so_REUSEADDR pozwoli po złym zamknięciu aplikacji od razu zwolnić socket, aby móc się znów szybko połączyć
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        self.v = 0
        self.a = 0
        self.v_dev = 0
        self.a_dev = 0
        self.voltage = [0, 0, 0, 0, 0]

        self.x = 0
        self.y = 0
        self.x_dev = 0
        self.y_dev = 0
        self.px_length = 1

        self.angle = 0
        self.angle_dev = 0

        self.last_received = time.time()

        self.first = False
        self.second = False
        self.ready = False
        self.angle_calibrated = False

    def try_establish_connection(self):
        if self.isRunning:
            try:
                self.client, self.addr = self.socket.accept()
                self.socket.setblocking(0)
                self.connected = True
                self.signal.info.emit("Connection established")
            except:
                self.signal.connection_error.emit("Cannot establish connection")

    def get_data(self):
        if self.isRunning:
            if not self.connected:
                self.try_establish_connection()
                return None
            else:
                imu_data = self.receive_data()
                if imu_data is None:
                    self.signal.wrong_frame_error.emit("IMU error: wrong data format")
                else:
                    if 'first' in imu_data:
                        self.first = True
                    elif 'second' in imu_data:
                        self.second = True
                    else:
                        angle, x, y = self.processed_data(imu_data)
                        self.signal.success.emit(angle, x, y)
                        self.ready = True
                    self.signal.info.emit("IMU: new data")

    def receive_data(self):
        self.complete = False
        ready = select.select([self.client], [], [], 1)
        while not self.complete:  # sprawdzewnie czasu od dostarczenia ostatniej danej
            if ready[0]:
                content = self.client.recv(1024).decode()  # odbiór danych
                self.complete = True
                if len(content) != 0:  # sprawdzenie czy odebrana dana jest pusta
                    return content
            else:
                self.connected = False
                self.client.close()
                self.socket.setblocking(1)
                self.signal.connection_error.emit("IMU error: Connection loss")
                return None

    def processed_data(self, content):
        dt = time.time() - self.last_received
        self.last_received = time.time()
        content = content.split()
        angle, v, a, voltage = float(content[0]), float(content[1]), float(content[2]), float(content[3])
        ds = (v - self.v_dev) * dt + a * dt ** 2 / 2
        self.x += ds * math.cos(math.radians(angle))
        self.y += ds * math.sin(math.radians(angle))
        self.v = v
        self.a = a
        self.angle = angle
        self.voltage.append(3.22 * voltage / (1024 / 5))  # Zamiana odczytu z 1024 bitowego przetwornika ADC na V
        self.voltage.pop(0)

        x = (math.cos(self.angle_dev) * self.x - math.sin(self.angle_dev) * self.y - self.x_dev) * 100 / self.px_length
        y = -(math.sin(self.angle_dev) * self.x + math.cos(self.angle_dev) * self.y - self.y_dev) * 100 / self.px_length
        angle = (self.angle - math.degrees(self.angle_dev) + 360) % 360

        return angle, x, y

    def update_dev(self, ctr_x, ctr_y, speed, px_length):
        self.px_length = px_length
        x = math.cos(self.angle_dev) * self.x - math.sin(self.angle_dev) * self.y
        y = math.sin(self.angle_dev) * self.x + math.cos(self.angle_dev) * self.y
        self.x_dev = x - ctr_x * px_length / 100
        self.y_dev = y + ctr_y * px_length / 100
        self.v_dev = self.v - speed
