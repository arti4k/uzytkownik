import time
import urllib.request
import threading
import cv2
import numpy as np
from PySide2.QtCore import *
from PySide2.QtGui import *

import qimage2ndarray


class dataset_CAMERA:
    def __init__(self):
        self.processed_frame = None
        self.contour = None
        self.threshold_camera = 255
        self.first_detection = False
        self.counter = 0
        self.f_img = True
        self.f_ctr_x = 0.
        self.ctr_x = 0.
        self.f_ctr_y = 0.
        self.ctr_y = 0
        self.px_length = 0
        self.speed = 0
        self.angle = 0
        self.time_prev = 0
        self.dt = 0
        self.box_corners = []
        self.angle2IMU = 0
        self.points = []
        self.ready = False

    def update(self, result):
        self.threshold_camera, self.first_detection, self.f_img, self.f_ctr_x, self.f_ctr_y, self.px_length = result
        pass


class CameraSignals(QObject):
    success = Signal(QImage, float, float)
    info = Signal(str)
    connection_error = Signal(str)
    wrong_frame_error = Signal(str)


class Camera(QObject):
    def __init__(self, url):
        super().__init__()
        print("Initializing camera connection...")
        self.url = url
        self.connected = False
        self.stream = None
        self.complete = False
        self.bytes = bytes()
        self.ready = False
        self.first_ctr_x = 0
        self.first_ctr_y = 0

        self.isRunning = True

        self.dataset = dataset_CAMERA()
        self.signal = CameraSignals()

    def try_establish_connection(self):
        if self.isRunning:
            try:
                self.stream = urllib.request.urlopen(self.url)
                print("Camera connection established")
                self.connected = True
                self.signal.info.emit("Connected")
            except:
                self.signal.connection_error.emit("Camera error: Reconnecting ...")

    def get_image(self):
        if self.isRunning:
            if not self.connected:
                self.try_establish_connection()
                return None
            else:
                if not self.dataset.f_img:
                    self.dataset.dt = time.time() - self.dataset.time_prev
                self.dataset.time_prev = time.time()
                frame = self.receive_data()
                if frame is None:
                    self.signal.wrong_frame_error.emit("Canera error: Wrong received frame format!")

                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                self.dataset.contour = frame.copy()
                self.dataset.processed_frame = process_frame(frame, self.dataset.threshold_camera)

                result = get_cnt(self.dataset)
                self.dataset.update(result)

                image = qimage2ndarray.array2qimage(self.dataset.contour)

                # TODO 2# tutaj przekazuje nadpisane centra z funkcji get cnt
                self.signal.success.emit(image, self.dataset.ctr_x, self.dataset.ctr_y)
        else:
            print("Stop device")

    def receive_data(self):

        self.complete = False
        while not self.complete:

            # TODO to jest brzydkie, ale dzięki temu jesteśmy w stanie stwierdzić, czy nastąpiło przerwanie
            # TODO połączenia, czy nie. Później - poprawić
            piece = self.stream.read(1024)
            self.bytes += piece

            if piece == b'\r\n' or piece == b'':
                self.connected = False
                self.stream = None
                self.signal.connection_error.emit("Camera error: Connection lost!")

            a = self.bytes.find(b'\xff\xd8')
            b = self.bytes.find(b'\xff\xd9')
            if a != -1 and b != -1:
                img = self.bytes[a:b + 2]
                self.bytes = self.bytes[b + 2:]
                self.complete = True
                try:
                    i = cv2.imdecode(np.fromstring(img, dtype=np.uint8), cv2.IMREAD_COLOR)
                    return i
                except:
                    print("parse image error")
                    return None

    def angle_for_IMU(self, first, second):
        if first and not second:
            self.dataset.points.append([self.dataset.ctr_x, -self.dataset.ctr_y])
        if second and not self.ready:
            data = np.array(self.dataset.points)
            if len(data) > 1:
                a = (len(data) * sum(data[:, 0] * data[:, 1]) - sum(data[:, 0]) * sum(data[:, 1])) / (
                                len(data) * sum(data[:, 0] ** 2) - sum(data[:, 0]) ** 2)
                delta_x = data[-1][0] - data[0][0]
                y_1 = a * data[0][0]
                y_2 = a * data[-1][0]
                delta_y = y_2 - y_1
                self.dataset.angle2IMU = (np.degrees(np.arctan2(delta_x, delta_y)) + 360) % 360
            self.ready = True
            self.dataset.ready = True
            self.dataset.points = []


def get_cnt(data):
    contours, _ = cv2.findContours(data.processed_frame, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    for cnt in contours:
        area = cv2.contourArea(cnt)
        area_min = 25000
        area_max = 50000
        data.counter += 1
        if area_max > area > area_min:
            data.counter = 0
            data.first_detection = True
            perimeter = cv2.arcLength(cnt, True)
            epsilon = 0.02 * perimeter
            approximation = cv2.approxPolyDP(cnt, epsilon, True)

            rect = cv2.minAreaRect(cnt)
            box = cv2.boxPoints(rect)
            box = np.int0(box)
            cv2.drawContours(data.contour, [box], 0, (0, 0, 255), 4)

            x, y, w, h = cv2.boundingRect(approximation)

            data.box_corners = box
            center_x = rect[0][0]
            center_y = rect[0][1]

            if data.f_img:
                if w > h:
                    a = w
                else:
                    a = h
                data.px_length = 36 / a
                data.f_ctr_x = center_x
                data.f_ctr_y = center_y
                data.ctr_x = center_x
                data.ctr_y = center_y
                data.f_img = False

            move_X = center_x - data.ctr_x
            move_Y = center_y - data.ctr_y
            if not data.dt == 0:
                data.speed = data.px_length * np.sqrt((move_X) ** 2 + (move_Y) ** 2) / (data.dt * 100)

            data.ctr_x = center_x
            data.ctr_y = center_y

            x_disp = (center_x - data.f_ctr_x) * data.px_length
            y_disp = (center_y - data.f_ctr_y) * data.px_length

            if data.ready:
                radian = np.deg2rad(180 - data.angle)
                scannedX = (center_x + np.sin(radian) * 250)
                scannedY = (center_y + np.cos(radian) * 250)
                cv2.line(data.contour, ((int(center_x), int(center_y))), (int(scannedX), int(scannedY)), (0, 255, 0))
                cv2.circle(data.contour, (int(scannedX), int(scannedY)), 5, (0, 255, 0), -1)

            red = (0, 0, 255)  # RGB

            cv2.putText(data.contour, "X displacement: " + str(int(x_disp)), (250, 25), cv2.QT_FONT_NORMAL, 0.7, red, 2)
            cv2.putText(data.contour, "Y displacement: " + str(int(y_disp)), (250, 50), cv2.QT_FONT_NORMAL, 0.7, red, 2)

            cv2.putText(data.contour, "Position x: " + str(int(center_x)), (25, 20), cv2.QT_FONT_NORMAL, 0.7, red, 2)
            cv2.putText(data.contour, "Position y: " + str(int(center_y)), (25, 50), cv2.QT_FONT_NORMAL, 0.7, red, 2)

            # cv2.putText(contour, "area: " + str(area), (25, 80), cv2.QT_FONT_NORMAL, 0.7, red, 2)
            # cv2.putText(contour, "approx: " + str(len(approximation)), (25, 110), cv2.QT_FONT_NORMAL, 0.7, red, 2)

            cv2.circle(data.contour, (int(center_x), int(center_y)), 5, (0, 0, 255), -1)
        elif data.counter > 75:
            warning = 'Loss of visibility, check if the robot is clearly visible'
            cv2.putText(data.contour, warning, (25, 100), cv2.QT_FONT_NORMAL, 0.6, (255, 0, 0), 2)
            if not data.first_detection:
                cv2.putText(data.contour, "Searching robot", (250, 75), cv2.QT_FONT_NORMAL, 0.6, (255, 0, 0), 2)
                if data.threshold_camera > 120:
                    data.threshold_camera = data.threshold_camera - 10
                    time.sleep(0.2)

    return data.threshold_camera, data.first_detection, data.f_img, data.f_ctr_x, data.f_ctr_y, data.px_length


def process_frame(frame, threshold):
    th_optimized = 0.5 * threshold

    blur = cv2.GaussianBlur(src=frame, ksize=(7, 7), sigmaX=1)
    gray = cv2.cvtColor(src=blur, code=cv2.COLOR_RGB2GRAY)
    canny = cv2.Canny(image=gray, threshold1=th_optimized, threshold2=th_optimized)
    dilate = cv2.dilate(src=canny, kernel=np.ones((9, 9)), iterations=3)

    return dilate
