# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'artik.ui'
##
## Created by: Qt User Interface Compiler version 5.14.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
    QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QKeySequence, QLinearGradient, QPalette, QPainter,
    QPixmap, QRadialGradient)
from PySide2.QtWidgets import *

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1300, 672)
        MainWindow.setMinimumSize(QSize(1300, 650))
        icon = QIcon()
        icon.addFile(u"artik.ico", QSize(), QIcon.Normal, QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.layoutWidget = QWidget(self.centralwidget)
        self.layoutWidget.setObjectName(u"layoutWidget")
        self.layoutWidget.setGeometry(QRect(0, 20, 1290, 567))
        self.verticalLayout = QVBoxLayout(self.layoutWidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.control_hBox_layout = QHBoxLayout()
        self.control_hBox_layout.setObjectName(u"control_hBox_layout")
        self.vLayout_imu = QVBoxLayout()
        self.vLayout_imu.setObjectName(u"vLayout_imu")
        self.imu_label = QLabel(self.layoutWidget)
        self.imu_label.setObjectName(u"imu_label")
        self.imu_label.setAlignment(Qt.AlignCenter)

        self.vLayout_imu.addWidget(self.imu_label)


        self.control_hBox_layout.addLayout(self.vLayout_imu)

        self.vLayout_camera = QVBoxLayout()
        self.vLayout_camera.setObjectName(u"vLayout_camera")
        self.camera_label = QLabel(self.layoutWidget)
        self.camera_label.setObjectName(u"camera_label")
        self.camera_label.setAlignment(Qt.AlignCenter)

        self.vLayout_camera.addWidget(self.camera_label)


        self.control_hBox_layout.addLayout(self.vLayout_camera)


        self.verticalLayout.addLayout(self.control_hBox_layout)

        self.plot_image_hBoxLayout = QHBoxLayout()
        self.plot_image_hBoxLayout.setObjectName(u"plot_image_hBoxLayout")
        self.plotter = MplWidget(self.layoutWidget)
        self.plotter.setObjectName(u"plotter")
        self.plotter.setMinimumSize(QSize(640, 480))

        self.plot_image_hBoxLayout.addWidget(self.plotter)

        self.image_viewer = QLabel(self.layoutWidget)
        self.image_viewer.setObjectName(u"image_viewer")
        self.image_viewer.setMinimumSize(QSize(640, 480))
        self.image_viewer.setStyleSheet(u"background-color:lightgrey")
        self.image_viewer.setAlignment(Qt.AlignCenter)

        self.plot_image_hBoxLayout.addWidget(self.image_viewer)


        self.verticalLayout.addLayout(self.plot_image_hBoxLayout)

        self.imu_angle_label = QLabel(self.centralwidget)
        self.imu_angle_label.setObjectName(u"imu_angle_label")
        self.imu_angle_label.setGeometry(QRect(650, 590, 641, 41))
        self.imu_connection_label = QLabel(self.centralwidget)
        self.imu_connection_label.setObjectName(u"imu_connection_label")
        self.imu_connection_label.setGeometry(QRect(10, 590, 631, 41))
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 1300, 21))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"Arti4k", None))
        self.imu_label.setText(QCoreApplication.translate("MainWindow", u"Establishing connection with IMU...", None))
        self.camera_label.setText(QCoreApplication.translate("MainWindow", u"Establishing connection with camera...", None))
        self.image_viewer.setText(QCoreApplication.translate("MainWindow", u"Placeholder for camera frames", None))
        self.imu_angle_label.setText("")
        self.imu_connection_label.setText("")
    # retranslateUi

from artik_gui_components import MplWidget