import glob
import socket
import sys
import traceback

from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtUiTools import QUiLoader

import numpy as np
import random
import collections
import time

from artik_imu import IMU, one_record_IMU
from artik_camera import Camera
from artik_gui_components import MplWidget, is_valid_ipv4_address, is_valid_port


class mainArtikWindow(QMainWindow):

    def __init__(self, imu_ip_socket, camera_url):
        QMainWindow.__init__(self)

        # Załaduj UI i wyświetl
        designer_file = QFile("artik.ui")
        designer_file.open(QFile.ReadOnly)
        loader = QUiLoader()
        loader.registerCustomWidget(MplWidget)

        self.ui = loader.load(designer_file, self)
        designer_file.close()

        grid_layout = QGridLayout()
        grid_layout.addWidget(self.ui)
        self.setLayout(grid_layout)
        self.ui.show()

        self.mainThread = (QThread.currentThread())

        # ================= IMU =================

        self.imu = IMU(imu_ip_socket)

        self.thread_imu = QThread(self.mainThread)
        self.thread_imu.start()
        self.imu.moveToThread(self.thread_imu)

        self.imu.signal.success.connect(self.process_imu_data)
        self.imu.signal.info.connect(self.display_message_imu)
        self.imu.signal.connection_error.connect(self.display_message_imu)
        self.imu.signal.wrong_frame_error.connect(self.display_message_imu)
        
        self.last_calibration = time.time()

        # ================= CAMERA =================

        self.camera = Camera(camera_url)

        self.thread_camera = QThread(self.mainThread)
        self.thread_camera.start()
        self.camera.moveToThread(self.thread_camera)

        self.camera.signal.success.connect(self.display_frame)
        self.camera.signal.info.connect(self.display_message_camera)
        self.camera.signal.connection_error.connect(self.display_message_camera)
        self.camera.signal.wrong_frame_error.connect(self.display_message_camera)

        # ================= TIMER =================

        self.fps_camera = 50.
        self.fps_imu = 130.

        self.timer_camera = QTimer()
        self.timer_camera.timeout.connect(self.camera.get_image)
        self.timer_camera.start(1000. / self.fps_camera)

        self.timer_imu = QTimer()
        self.timer_imu.timeout.connect(self.imu.get_data)
        self.timer_imu.start(1000. / self.fps_imu)

        self.no_data_imu_timer = QTimer()
        self.no_data_imu_timer.start(1000)


        # ================= PLOT & DATA =================

        self.ui.plotter.canvas.axes.set_xlim(0, 640)
        self.ui.plotter.canvas.axes.set_ylim(0, 480)
        self.ui.plotter.canvas.figure.gca().invert_yaxis()

        self.chartLine_camera = []
        self.chartLine_imu = []

        self.maxlen = 100
        self.angle_imu = collections.deque(maxlen=self.maxlen)
        self.x_imu = collections.deque(maxlen=self.maxlen)
        self.y_imu = collections.deque(maxlen=self.maxlen)

        self.x_camera = collections.deque(maxlen=self.maxlen)
        self.y_camera = collections.deque(maxlen=self.maxlen)

    @Slot(float, float, float)
    def process_imu_data(self, angle, x, y):
        self.angle_imu.append(angle)
        self.x_imu.append(x)
        self.y_imu.append(y)

        self.update_graph_imu()
        self.no_data_imu_timer.start(1000)

    def update_graph_imu(self):
        if not self.chartLine_imu:
            self.chartLine_imu, = self.ui.plotter.canvas.axes.plot(self.x_imu, self.y_imu,
                                                                   '-o', markersize=1, label='imu')
            self.ui.plotter.canvas.axes.legend(loc='upper right')

        self.chartLine_imu.set_data(self.x_imu, self.y_imu)
        self.ui.plotter.canvas.draw()
        if self.angle_imu:
            self.ui.imu_angle_label.setText(
                "Voltage: {0:5.2f}, Angle: {1:6.2f}".format(np.mean(self.imu.voltage), self.angle_imu[-1]))

    def update_graph_camera(self, x, y):
        self.x_camera.append(x)
        self.y_camera.append(y)

        if not self.chartLine_camera:
            self.chartLine_camera, = self.ui.plotter.canvas.axes.plot(self.x_camera, self.y_camera,
                                                                      '-o', markersize=1, label='kamera')
            self.ui.plotter.canvas.axes.legend(loc='upper right')

        self.chartLine_camera.set_data(self.x_camera, self.y_camera)
        self.ui.plotter.canvas.draw()

    @Slot(QImage, float, float)
    def display_frame(self, frame, x, y):
        if self.imu.ready and not self.imu.angle_calibrated and self.camera.ready:
            self.imu.angle_dev = np.radians(self.imu.angle - self.camera.dataset.angle2IMU)
            self.imu.angle_calibrated = True
        elif self.imu.ready and self.imu.angle_calibrated and time.time() - self.last_calibration > 0.1:
            self.imu.update_dev(self.camera.dataset.ctr_x, self.camera.dataset.ctr_y, self.camera.dataset.speed,
                                self.camera.dataset.px_length)
            self.last_calibration = time.time()
        
        self.ui.image_viewer.setPixmap(QPixmap.fromImage(frame))
        self.update_graph_camera(x, y)

    @Slot(str)
    def display_message_camera(self, message):
        self.ui.camera_label.clear()
        self.ui.camera_label.setText(message)

    @Slot(str)
    def display_message_imu(self, message):
        if self.imu.first:
            self.camera.ready = False
            self.imu.angle_calibrated = False
            self.imu.ready = False
            self.camera.angle_for_IMU(self.imu.first, self.imu.second)
        if self.camera.ready:
            self.imu.first = False
            self.imu.second = False
            self.camera.dataset.first_saved = False
        self.ui.imu_label.clear()
        self.ui.imu_label.setText(message)
        self.camera.dataset.angle = (self.imu.angle - np.degrees(self.imu.angle_dev) + 360 % 360)

    def close_threads(self):

        self.timer_camera.stop()
        self.timer_imu.stop()

        self.camera.isRunning = False
        self.imu.isRunning = False
        
        try:
            self.imu.client.close()
        except:
            print("Client doesnt exists")

        del self.camera.signal
        del self.imu.signal


        self.thread_camera.moveToThread(self.mainThread)
        self.thread_imu.moveToThread(self.mainThread)
        # del self.camera
        # del self.imu

        del self.thread_camera
        del self.thread_imu




class start_gui(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)

        self.cancel_main_gui = True
        for name in glob.glob('robot_data.txt'):
            text_file = open(name, "r")
            lines = text_file.readlines()
            if lines[0] == "program verison v0.1 \n" and len(lines) < 4:
                data = lines[2].split(',')
                if len(data) < 5:
                    self.IPV4_imu, self.Port_imu, self.IPV4_camera, self.Port_camera = data[0], data[1], data[2], data[
                        3]
                else:
                    self.IPV4_imu, self.Port_imu, self.IPV4_camera, self.Port_camera = "0.0.0.0", "0", "0.0.0.0", "0"
            else:
                self.IPV4_imu, self.Port_imu, self.IPV4_camera, self.Port_camera = "0.0.0.0", "0", "0.0.0.0", "0"
        if not glob.glob('robot_data.txt'):
            self.IPV4_imu, self.Port_imu, self.IPV4_camera, self.Port_camera = "0.0.0.0", "0", "0.0.0.0", "0"

        # Załaduj UI i wyświetl
        designer_file = QFile("start_artik.ui")
        designer_file.open(QFile.ReadOnly)
        loader = QUiLoader()
        loader.registerCustomWidget(MplWidget)

        self.ui = loader.load(designer_file, self)
        designer_file.close()

        self.ui.setFixedSize(QSize(self.ui.width(), self.ui.height()))

        grid_layout = QGridLayout()
        grid_layout.addWidget(self.ui)
        self.setLayout(grid_layout)
        self.ui.show()

        self.ui.lineEdit.setText(self.IPV4_imu)
        self.ui.lineEdit_2.setText(self.Port_imu)
        self.ui.lineEdit_3.setText(self.IPV4_camera)
        self.ui.lineEdit_4.setText(self.Port_camera)

        self.ui.pushButton.clicked.connect(self.button_ok)
        self.ui.pushButton_2.clicked.connect(self.button_cancel)


    @Slot()
    def button_ok(self):
        self.ui.label.setText('Your imu IP is: ' + str(self.ui.lineEdit.text()))
        self.ui.label_2.setText('Your imu port is: ' + str(self.ui.lineEdit_2.text()))
        self.ui.label_3.setText('Your camera IP is: ' + str(self.ui.lineEdit_3.text()))
        self.ui.label_4.setText('Your camera port is: ' + str(self.ui.lineEdit_4.text()))

        if not (is_valid_ipv4_address(self.ui.lineEdit.text()) and is_valid_ipv4_address(
                self.ui.lineEdit_3.text()) and is_valid_port(str(self.ui.lineEdit_2.text())) and is_valid_port(
                str(self.ui.lineEdit_4.text()))):
            if not is_valid_ipv4_address(self.ui.lineEdit.text()):
                self.ui.label.setText('Your imu IP is not valid ')
            if not is_valid_ipv4_address(self.ui.lineEdit_3.text()):
                self.ui.label.setText('Your camera ip is not valid')
            if not is_valid_port(self.ui.lineEdit_2.text()):
                self.ui.label.setText('Your imu port is not valid')
            if not is_valid_port(self.ui.lineEdit_4.text()):
                self.ui.label.setText('Your camera port is not valid')
            return

        filestring = f"program verison v0.1 \n" \
                     f"robot imu and camera ip/port data: \n" \
                     f"{self.ui.lineEdit.text()},{self.ui.lineEdit_2.text()},{self.ui.lineEdit_3.text()},{self.ui.lineEdit_4.text()}"
        text_file = open("robot_data.txt", "w")
        text_file.write(filestring)
        text_file.close()
        self.close()

    @Slot()
    def button_cancel(self):
        self.cancel_main_gui = False
        self.close()




def main():
    QCoreApplication.setAttribute(Qt.AA_ShareOpenGLContexts)
    app = QApplication([])
    window = start_gui()
    app.exec_()
    if window.cancel_main_gui == True:

        camera_url = 'http://' + window.ui.lineEdit_3.text() + ':' \
                     + str(window.ui.lineEdit_4.text()) + '/stream.mjpg'  # 'http://192.168.0.136:8000/stream.mjpg'

        imu_ip = window.ui.lineEdit.text()
        imu_socket = int(window.ui.lineEdit_2.text())

        window = mainArtikWindow((imu_ip, imu_socket), camera_url)

        app.aboutToQuit.connect(window.close_threads)
        app.exec_()


if __name__ == '__main__':
    main()
