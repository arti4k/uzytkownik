# -*- coding: utf-8 -*-
import tkinter
from tkinter import IntVar, StringVar, BooleanVar
import time
import threading
import queue
import PIL.Image, PIL.ImageTk
import cv2
import glob


import matplotlib.pyplot as plt

plt.style.use('ggplot')

from artik_camera import dataset_CAMERA, Camera, process_frame, get_cnt
from imu_class_test import IMU as IMU_TEST
from artik_imu import IMU
from artik_imu import one_record_IMU, dataset_IMU
from artik_gui_components import *


class GUI:
    def __init__(self, master, queue_camera, queue_imu, thread_imu_operator, thread_camera_operator):

        self.master = master

        self.queue_camera = queue_camera

        self.dataset_camera = dataset_CAMERA()

        self.thread_imu_operator = thread_imu_operator
        self.thread_camera_operator = thread_camera_operator

        self.queue_imu = queue_imu
        self.dataset_imu = dataset_IMU()

        self.position_plot = artikPlot(master, (6, 5), 50, 5)
        self.left_plot = artikPlot(master, (7, 4), 5, 525)
        self.right_plot = artikPlot(master, (7, 4), 710, 525)

        self.position_plot.ax.set_xlabel('Position: {}'.format(''))
        self.left_plot.set_magnetometer_description()
        self.right_plot.set_magnetometer_description()


        self.canvas = artikCanvas(master, 735, 5)
        # "bezużyteczna zmienna, służy jedynie do zapobiegania czyszczenia garbage collectora"
        self.image = None

        self.chartLines_mx = []
        self.chartLines_mx_m_x = []

        self.chartLines_my = []
        self.chartLines_my_m_y = []

        self.chartLines_yaw_g = []
        self.chartLines_yaw_m = []

        # Setup GUI - można tutaj, można przed wywołaniem w main
        # klasa i tak odwołuje się do mastera
        self.button_imu_frame = self.frame = tkinter.Frame(self.master)
        self.button_imu_frame.place(x=665, y=5)
        self.button_imu = tkinter.Button(self.button_imu_frame, text='Toggle imu',
                                         command=lambda: self.thread_imu_operator('toggle'))
        self.button_imu.pack()

        self.button_camera_frame = self.frame = tkinter.Frame(self.master)
        self.button_camera_frame.place(x=655, y=35)
        self.button_camera = tkinter.Button(self.button_camera_frame, text='Toggle camera',
                                            command=lambda: self.thread_camera_operator('toggle'))
        self.button_camera.pack()

        self.master.protocol('WM_DELETE_WINDOW', self.delete_window)

    def livePlot(self, x, imuData, chartLines, which_plot):
        if not chartLines:
            chartLines, = which_plot.ax.plot(x, imuData, alpha=0.8)
        chartLines.set_data(x, imuData)
        which_plot.ax.relim()
        which_plot.ax.autoscale_view(True, True, True)
        which_plot.graph.draw()
        return chartLines

    def processIncoming(self):
        # Obsłuż dane z kolejki, o ile jakieś występują
        while self.queue_imu.qsize() or self.queue_camera.qsize():
            try:
                one_record_imu = one_record_IMU(self.queue_imu.get(0))
                if one_record_imu.mode == 'lpf':
                    self.dataset_imu.append_lpf(one_record_imu)
                elif one_record_imu.mode == 'com':
                    self.dataset_imu.append_com(one_record_imu)

                old, new = self.dataset_imu.which_graph

                if old == 'lpf' and new == 'com':
                    self.left_plot.remove_lines()

                    if self.chartLines_yaw_m != []:
                        self.left_plot.ax.lines.extend([self.chartLines_yaw_m, self.chartLines_yaw_g])

                    self.left_plot.ax.set_ylabel('Angle')
                    self.left_plot.ax.set_ylim(0, 360)

                elif old == 'com' and new == 'lpf':
                    self.left_plot.remove_lines()

                    if self.chartLines_mx != []:
                        self.left_plot.ax.lines.extend([self.chartLines_mx, self.chartLines_mx_m_x])

                    self.left_plot.ax.set_ylabel('Magnetometer')
                    self.left_plot.ax.set_ylim(-32767, 32767)
                else:
                    pass

                if new == 'lpf':

                    self.chartLines_mx = self.livePlot(self.dataset_imu.x_lpf, self.dataset_imu.mag_x,
                                                       self.chartLines_mx, self.left_plot)

                    self.chartLines_mx_m_x = self.livePlot(self.dataset_imu.x_lpf, self.dataset_imu.mag_x_max,
                                                           self.chartLines_mx_m_x, self.left_plot)

                    self.chartLines_my = self.livePlot(self.dataset_imu.x_lpf, self.dataset_imu.mag_y,
                                                       self.chartLines_my, self.right_plot)

                    self.chartLines_my_m_y = self.livePlot(self.dataset_imu.x_lpf, self.dataset_imu.mag_y_max,
                                                           self.chartLines_my_m_y, self.right_plot)

                elif new == 'com':
                    self.chartLines_yaw_g = self.livePlot(self.dataset_imu.x_com, self.dataset_imu.yaw_g,
                                                          self.chartLines_yaw_g, self.left_plot)
                    self.chartLines_yaw_m = self.livePlot(self.dataset_imu.x_com, self.dataset_imu.yaw_m,
                                                          self.chartLines_yaw_m, self.left_plot)

                else:
                    print("Process Incoming: Wrong dataframe!")

            except queue.Empty:
                pass

            # try-except są osobno, gdyż jak zamknie się jeden z wątków, to drugi dalej się wykonuje
            try:
                frame = self.queue_camera.get(0)

                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                self.dataset_camera.contour = frame.copy()
                self.dataset_camera.processed_frame = process_frame(frame, self.dataset_camera.threshold_camera)

                result = get_cnt(self.dataset_camera)
                self.dataset_camera.update(result)

                photo = PIL.ImageTk.PhotoImage(image=PIL.Image.fromarray(self.dataset_camera.contour))
                # zapisuje do nieużywanego self.image by zapobiec usunięciu przez garbage collector
                # w przeciwnym wypadku tkinter Canvas nie wyświetlał nic
                self.image = photo

                self.canvas.update_image(photo)

            except queue.Empty:
                pass

    def delete_window(self):
        # custom obsługa zamknięcia aplikacji
        # naciśnięcie przycisków powoduje zatrzymanie wątków imu i kamery
        self.thread_imu_operator('exit')
        self.thread_camera_operator('exit')
        self.master.destroy()


class ThreadedClient:

    def __init__(self, master, camera, imu):
        """
        to w tej klasie wywołuje się GUI, przez to kontrola nad wątkami:
        Tkinera - mainloop
        Imu
        Kamery
        znajduje się w jednym miejscu
        """

        self.master = master
        self.camera = camera
        self.imu = imu

        # kolejki do obsługi danych
        self.queue_imu = queue.Queue()
        self.queue_camera = queue.Queue()

        # UTWORZENIE OBIEKTU GUI
        self.gui = GUI(master, self.queue_camera, self.queue_imu, self.end_thread_imu, self.end_thread_camera)

        # ustawienie flag i wątków do pracy
        self.running_app = True
        self.running_imu = True
        self.running_camera = True

        self.receive_imu = False
        self.receive_camera = False
        self.thread_imu = threading.Thread(target=self.thread_imu_function)
        self.thread_camera = threading.Thread(target=self.thread_camera_function)

        self.thread_imu.start()
        self.thread_camera.start()

        # funkcja do okresowego sprawdzania czy są nowe dane
        self.periodicCall()

    def periodicCall(self):

        self.gui.processIncoming()
        if (not self.running_imu) and (not self.running_camera) and (not self.running_app):
            import sys
            sys.exit(1)

        # pierwszy argument - czas w ms odświeżania danych
        self.master.after(50, self.periodicCall)

    def thread_imu_function(self):
        while self.running_imu:
            if self.receive_imu:
                if imu.status:
                    msg = imu.GetIMUData()
                    self.queue_imu.put(msg)
                    # sleep jest brzydki, ale konieczny
                    # w przeciwnym wypadku jeden wątek przejmie kontrolę i nie dopuści innych
                    time.sleep(0.15)
                else:
                    imu.try_establish_connection()
                    time.sleep(0.5)
            else:
                time.sleep(0.5)

    def end_thread_imu(self, mode='toggle'):
        if mode == 'toggle':
            self.receive_imu = not self.receive_imu
        elif mode == 'exit':
            self.running_imu = not self.running_imu
        else:
            print('Wrong mode!')

    def thread_camera_function(self):
        while self.running_camera:
            if self.receive_camera:
                if camera.connected:
                    image = camera.receive_data()
                    self.queue_camera.put(image)
                    time.sleep(0.1)
                else:
                    camera.try_establish_connection()
                    time.sleep(0.5)
            else:
                time.sleep(0.5)

    def end_thread_camera(self, mode):

        if mode == 'toggle':
            self.receive_camera = not self.receive_camera
        elif mode == 'exit':
            self.running_camera = False
        else:
            print('Wrong mode!')


class start_gui:

    def __init__(self, master):
        self.master = master
        # Wczytywanie pliku z danymi
        for name in glob.glob('robot_data.txt'):
            text_file = open(name, "r")
            lines = text_file.readlines()
            if lines[0] == "program verison v0.1 \n" and len(lines) < 4:
                data = lines[2].split(',')
                if len(data) < 5:
                    self.IPV4_imu, self.Port_imu, self.IPV4_camera, self.Port_camera = self.set_variable(data[0], data[1], data[2], data[3])
                else:
                    self.IPV4_imu, self.Port_imu, self.IPV4_camera, self.Port_camera = self.set_variable("0.0.0.0", 0, "0.0.0.0", 0)
            else:
                self.IPV4_imu, self.Port_imu, self.IPV4_camera, self.Port_camera = self.set_variable("0.0.0.0", 0, "0.0.0.0", 0)

        if not glob.glob('robot_data.txt'):
            self.IPV4_imu, self.Port_imu, self.IPV4_camera, self.Port_camera = self.set_variable("0.0.0.0", 0, "0.0.0.0", 0)

        self.cancel_main_gui = BooleanVar(choice_gui_root, False)
        self.mode = IntVar()

        self.frame_ok = tkinter.Frame(self.master)
        self.frame_ok.place(x=190, y=170)
        self.button_save_option = tkinter.Button(self.frame_ok, text='   ok   ',
                                                 command=lambda *args: self.button_ok(self.IPV4_imu, self.Port_imu, self.IPV4_camera, self.Port_camera, self.cancel_main_gui))
        self.button_save_option.pack()

        self.frame_cancel = tkinter.Frame(choice_gui_root)
        self.frame_cancel.place(x=240, y=170)
        self.button_cancel_gui = tkinter.Button(self.frame_cancel, text=' cancel ', command=self.button_cancel)
        self.button_cancel_gui.pack()

        self.label_imu_ip = tkinter.Label(self.master, text="Enter imu IP: ")
        self.label_imu_ip.place(x=20, y=0)
        self.nameEntered_imu_ip = tkinter.Entry(self.master, width=15, textvariable=self.IPV4_imu)
        self.nameEntered_imu_ip.place(x=20, y=20)

        self.label_imu_port = tkinter.Label(self.master, text="Enter imu port: ")
        self.label_imu_port.place(x=20, y=50)
        self.nameEntered_imu_port = tkinter.Entry(self.master, width=15, textvariable=self.Port_imu)
        self.nameEntered_imu_port.place(x=20, y=70)

        self.label_camera_ip = tkinter.Label(self.master, text="Enter camera ip: ")
        self.label_camera_ip.place(x=20, y=100)
        self.nameEntered_camera_ip = tkinter.Entry(self.master, width=15, textvariable=self.IPV4_camera)
        self.nameEntered_camera_ip.place(x=20, y=120)

        self.label_camera_port = tkinter.Label(self.master, text="Enter camera port: ")
        self.label_camera_port.place(x=20, y=150)
        self.nameEntered_camera_port = tkinter.Entry(self.master, width=15, textvariable=self.Port_camera)
        self.nameEntered_camera_port.place(x=20, y=170)
        
        self.frame_rb_user = tkinter.Frame(self.master)
        self.frame_rb_user.place(x=180, y=100)
        self.RB_user = tkinter.Radiobutton(self.frame_rb_user, text="User mode", variable=self.mode, value=0)
        self.RB_user.pack()

        self.frame_rb_dev = tkinter.Frame(choice_gui_root)
        self.frame_rb_dev.place(x=180, y=120)
        self.RB_dev = tkinter.Radiobutton(self.frame_rb_dev, text="Developer mode", variable=self.mode, value=1)
        self.RB_dev.pack()
        
    # Przycisk zatwierdzający wpisane dane oraz zapisujący je do pliku
        def button_ok(self, IPV4_imu, Port_imu, IPV4_camera, Port_camera, cancel_main_gui):
            self.label_imu_ip.configure(text='Your imu IP is: ' + str(IPV4_imu.get()))
            self.label_imu_port.configure(text='Your port is: ' + str(Port_imu.get()))
            self.label_camera_ip.configure(text='Your camera ip is: ' + str(IPV4_camera.get()))
            self.label_camera_port.configure(text='Your port is: ' + str(Port_camera.get()))

            if not (self.is_valid_ipv4_address(IPV4_imu.get()) and self.is_valid_ipv4_address(IPV4_camera.get()) and self.is_valid_port(str(Port_imu.get())) and self.is_valid_port(str(Port_camera.get()))):
                if not self.is_valid_ipv4_address(IPV4_imu.get()):
                    self.label_imu_ip.configure(text='Your imu IP is not valid ')
                if not self.is_valid_ipv4_address(IPV4_camera.get()):
                    self.label_camera_ip.configure(text='Your camera ip is not valid')
                if not self.is_valid_port(Port_imu.get()):
                    self.label_imu_port.configure(text='Your imu port is not valid')
                if not self.is_valid_port(Port_camera.get()):
                    self.label_camera_port.configure(text='Your camera port is not valid')
                return

            filestring = f"program verison v0.1 \n" \
                         f"robot imu and camera ip/port data: \n" \
                         f"{IPV4_imu.get()},{Port_imu.get()},{IPV4_camera.get()},{Port_camera.get()}"
            text_file = open("robot_data.txt", "w")
            text_file.write(filestring)
            text_file.close()
            cancel_main_gui.set(True)
            self.master.destroy()
        
    def button_cancel(self):
        self.master.destroy()
    # Funkcja ustawiająca dane
    def set_variable(self, set_IPV4_imu, set_Port_imu, set_IPV4_camera, set_Port_camera):
        Port_imu = IntVar(self.master, value=int(set_Port_imu))
        Port_camera = IntVar(self.master, value=int(set_Port_camera))
        IPV4_imu = StringVar(self.master, set_IPV4_imu)
        IPV4_camera = StringVar(self.master, set_IPV4_camera)

        return IPV4_imu, Port_imu, IPV4_camera, Port_camera
        
    def is_valid_ipv4_address(self, address):
        try:
            socket.inet_pton(socket.AF_INET, address)
        except AttributeError:
            try:
                socket.inet_aton(address)
            except socket.error:
                return False
            return address.count('.') == 3
        except socket.error:
            return False

        return True

    def is_valid_port(self, port):
        try:
            port = int(port)
        except ValueError:
            return False
        try:
            if 1 <= port <= 65535:
                return True
            else:
                raise ValueError
        except ValueError:
            return False
        
        
if __name__ == "__main__":
    
    # tworzenie okna początkowego
    choice_gui_root = tkinter.Tk()
    choice_gui_root.geometry("300x200")
    choice_gui_root.resizable(0, 0)
    choice_gui = start_gui(choice_gui_root)
    choice_gui_root.mainloop()

    if choice_gui.cancel_main_gui.get() == True:
        # tworzenie korzenia - master
        main_gui_root = tkinter.Tk()
        main_gui_root.geometry("1415x960")
        main_gui_root.resizable(0, 0)
        # otwieranie odpowiedniej wersji okna głównego
        if choice_gui.mode.get() == 0:
            imu = IMU(choice_gui.IPV4_imu.get(), choice_gui.Port_imu.get())
        elif choice_gui.mode.get() == 1:
            imu = IMU_TEST(choice_gui.IPV4_imu.get(), choice_gui.Port_imu.get())
        # tworzenie klienta który odpala wątki kamery i imu
        camera = Camera('http://' + choice_gui.IPV4_camera.get()+':'+str(choice_gui.Port_camera.get())+'/stream.mjpg') # 'http://192.168.0.136:8000/stream.mjpg'
        client = ThreadedClient(main_gui_root, camera, imu)
        main_gui_root.mainloop()